// secp256k1
// ecies

var bitcore = require('bitcore-lib');
var ECIES = require('bitcore-ecies');
var BS58 = require('bs58');

module.exports.encrypt = function (key, data) {
  var sec = new Buffer(BS58.decode(key));

  var alicePrivateKey = new bitcore.PrivateKey.fromObject(sec);
  var alicePublicKey = new bitcore.PublicKey.fromPrivateKey(alicePrivateKey);

  var cypher1 = ECIES().privateKey(alicePrivateKey).publicKey(alicePublicKey);
  return cypher1.encrypt(data);
}

module.exports.address = function (key) {
  try {
    var sec = new Buffer(BS58.decode(key));

    var alicePrivateKey = new bitcore.PrivateKey.fromObject(sec);
    var alicePublicKey = new bitcore.PublicKey.fromPrivateKey(alicePrivateKey);

    return alicePublicKey.toAddress();
  } catch(e) {
    return null;
  }
}

module.exports.decrypt = function (key, enc) {  
  var sec = new Buffer(BS58.decode(key))

  var alicePrivateKey = new bitcore.PrivateKey.fromObject(sec);
  var alicePublicKey = new bitcore.PublicKey.fromPrivateKey(alicePrivateKey);

  var cypher1 = ECIES().privateKey(alicePrivateKey).publicKey(alicePublicKey);
  return cypher1.decrypt(enc);
}
